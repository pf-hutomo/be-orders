import sys
sys.path.append('')

import os
from apis import models

import pandas as pd

def test_file_exist():
  # is file exists test
  assert os.open('./data/customers.csv', os.O_RDONLY)
  assert os.open('./data/orders.csv', os.O_RDONLY)
  assert os.open('./data/deliveries.csv', os.O_RDONLY)
  assert os.open('./data/customer_companies.csv', os.O_RDONLY)
  assert os.open('./data/order_items.csv', os.O_RDONLY)

def test_compiling_data():
  # compiling order test
  assert models.Order.order_with_customer_details() is not None
  assert models.Order.sum_of_orders_total_amount() is not None
  assert models.Order.sum_of_orders_delivered_amount() is not None
  assert models.Order.compile() is not None
