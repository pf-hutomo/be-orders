import sys
sys.path.append('')

import os
import pytest
from apis import app

@pytest.fixture
def client():
  app.app.config.update({
    "TESTING": True,
  })
  with app.app.test_client() as client:
    yield client

def test_path_root(client):
  response = client.get("/")
  assert response.status_code == 400
  assert response.json is None

def test_path_orders(client):
  response = client.get("/orders")
  assert response.status_code == 200
  assert response.json is not None