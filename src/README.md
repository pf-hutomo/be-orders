please notes before run the test or api:
- make sure "python" have already installed on the system
- make sure to install all the dependencies are in "requirements.txt" file
- for making the apps always run, please using the desired process manager like "supervisor"
- I'm using virtual environment for python, that can be activate using this commend; source /path/to/venv/bin/activate

## how to run the test?
### Test API
- open file apis/app.py, change line 2 become: from apis.orders import OrderController
- open file apis/orders.py, change line 1 become: from apis.models import Order
- from root folder (src) run the following command: python -m pytest tests/test_apis.py

### Test Models
- from root folder (src) run the following command: python -m pytest tests/test_models.py

## how to run the API?
- from root folder (src) run the following command: python apis/app.py