from flask import Flask, jsonify, request
from flask_cors import CORS
from orders import OrderController

app = Flask(__name__)
CORS(app)

@app.route("/", methods=['GET'])
def url_not_found():
  return "Page not found", 400

# endpoint to receive all the orders
@app.route("/orders", methods=['GET'])
def list_orders():
  return jsonify(OrderController.get_all_orders()), 200

if __name__ == "__main__":
  app.run()