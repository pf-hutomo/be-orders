import sys
sys.path.append('')

import os
import pandas as pd
import numpy as np

"""
this file is use for manipulate and prepare the data models
read csv using pandas
"""
df_orders = pd.read_csv('./data/orders.csv')
df_customers = pd.read_csv('./data/customers.csv')
df_customer_companies = pd.read_csv('./data/customer_companies.csv')
df_deliveries = pd.read_csv('./data/deliveries.csv')
df_orders_items = pd.read_csv('./data/order_items.csv')

class Order():

  @classmethod
  def compile(cls):
    customer_details = cls.order_with_customer_details()
    sum_of_total = cls.sum_of_orders_total_amount()
    sum_of_delivered = cls.sum_of_orders_delivered_amount()

    df_amount = pd.merge(sum_of_total, sum_of_delivered, how='left', on='order_id')
    df = customer_details.merge(df_amount, left_on='id', right_on='order_id')
    df['delivered_amount'] = np.where(df['delivered_amount'].isnull(), 0, df['delivered_amount'])

    # drop unused columns
    df_temp = df.drop(['credit_cards','password', 'login', 'user_id', 'company_id'], axis=1)

    return df_temp.to_dict(orient='records')

  @classmethod
  def order_with_customer_details(cls):
    """
    joining table orders and customers
    """
    df_oc = df_orders.merge(df_customers, left_on='customer_id', right_on='user_id')

    """
    completing join orders and customer with their company
    """
    df_orders_with_customers = pd.merge(df_oc, df_customer_companies, how='left', on='company_id')
    return df_orders_with_customers

  @classmethod
  def sum_of_orders_total_amount(cls):
    df_amount = df_orders_items.merge(df_orders, left_on='order_id', right_on='id')
    df_amount['price_per_unit'] = np.where(df_amount['price_per_unit'].isnull(), 0, df_amount['price_per_unit'])
    df_amount['total_amount'] = round(df_amount['price_per_unit'] * df_amount['quantity'],2)

    collect_product = df_orders_items.groupby('order_id')['product'].apply(list)

    sum_keys = ['order_name','total_amount']
    res = df_amount.groupby('order_id')[sum_keys].sum().reset_index()
    res = res.merge(collect_product, left_on='order_id', right_on='order_id')

    return res

  @classmethod
  def sum_of_orders_delivered_amount(cls):
    df_amount = df_deliveries.merge(df_orders_items, left_on='order_item_id', right_on='id')
    df_amount['price_per_unit'] = np.where(df_amount['price_per_unit'].isnull(), 0, df_amount['price_per_unit'])
    df_amount['delivered_amount'] = round(df_amount['price_per_unit'] * df_amount['delivered_quantity'],2)

    sum_keys = ['delivered_amount']
    res = df_amount.groupby('order_id')[sum_keys].sum().reset_index()

    return res

order = Order.sum_of_orders_total_amount()
print(order.to_string())