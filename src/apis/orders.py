from models import Order

# using a controller, just a sample if implementing in the real world program
# the models should be called from the controller. Not directly from the main program
class OrderController():
  @classmethod
  def get_all_orders(cls):
    return Order.compile()